# Editors

## IDEs

* [PyCharm](https://www.jetbrains.com/pycharm/) by JetBrains (All Platforms)
* [Wingware](http://www.wingware.com/) (All Platforms)

If you don’t want to go near IDEs and are happy with text editors and working in command line:

* [Atom Editor](https://atom.io/) (All Platforms)
* [Sublime Text](http://www.sublimetext.com/) (All Platforms)
* [TextMate](http://macromates.com/) (Mac only)
* [Visual Studio Code](https://code.visualstudio.com/download) (All Platforms)
